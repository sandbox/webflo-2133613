<?php
use Drupal\user\Entity\User;

/**
 * @file
 * Provides token-based name displays for users.
 *
 * @todo Add a 'view realname' permission enabled by default
 * @todo Allow users to login with their real name
 * @todo Disable the username field
 */

/**
 * @defgroup realname Real name API
 */

/**
 * Implements hook_permission().
 */
function realname_permission() {
  $permissions['administer realname'] = array(
    'title' => t('Administer Real Name configuration.'),
  );
  return $permissions;
}

/**
 * Implements hook_menu().
 */
function realname_menu() {
  $items['admin/config/people/realname'] = array(
    'title' => 'Real name',
    'description' => 'Use tokens to configure how user names are displayed.',
    'route_name' => 'realname_settings',
  );

  return $items;
}

/**
 * Implements hook_user_format_name_alter().
 */
function realname_user_format_name_alter(&$name, $account) {
  static $in_username_alter = FALSE;
  $uid = $account->id();

  // Don't alter anonymous users or objects that do not have any user ID.
  if(empty($uid)) return;

    // Real name was loaded/generated via hook_user_load(), so re-use it.
  if (isset($account->realname)) {
    if (drupal_strlen($account->realname)) {
      // Only if the real name is a non-empty string is $name actually altered.
      $name = $account->realname;
    }
    return;
  }

  // Real name was not yet available for the account so we need to generate it.
  // Because tokens may call format_username() we need to prevent recursion.
  if (!$in_username_alter) {
    $in_username_alter = TRUE;

    // If $account->realname was undefined, then the user account object was
    // not properly loaded. We must enforce calling user_load().
    if ($realname_account = user_load($uid)) {
      realname_user_format_name_alter($name, $realname_account);
    }

    $in_username_alter = FALSE;
  }
}

/**
 * Implements hook_user_load().
 */
function realname_user_load(array $accounts) {
  $realnames = realname_load_multiple($accounts);

  foreach ($realnames as $uid => $realname) {
    $accounts[$uid]->realname = $realname;
  }
}


/**
 * Implements hook_user_update().
 */
function realname_user_update($account) {
  // Since user data may have changed, delete the existing realname.
  realname_delete($account->id());
}

/**
 * Implements hook_user_delete().
 */
function realname_user_delete($account) {
  realname_delete($account->id());
}

/**
 * Implements hook_user_operations().
 */
function realname_user_operations() {
  $operations['realname_update'] = array(
    'label' => t('Update real name'),
    'callback' => 'realname_user_operations_realname_update',
  );
  return $operations;
}

/**
 * Callback function for admin mass generating user real names.
 *
 * @param $uids
 *   An array of user IDs.
 */
function realname_user_operations_realname_update(array $uids) {
  $accounts = user_load_multiple($uids);
  foreach ($accounts as $account) {
    realname_delete($account->id());
  }
}

/**
 * @addtogroup realname
 * @{
 */

/**
 * Loads a real name.
 *
 * @param $uid
 *   A user account object.
 * @return
 *   The user's generated real name.
 */
function realname_load(stdClass $account) {
  $realnames = realname_load_multiple(array($account->id() => $account));

  return reset($realnames);
}

/**
 * Loads multiple real names.
 *
 * @param $accounts
 *   An array of user account objects keyed by user ID.
 * @return
 *   An array of real names keyed by user ID.
 */
function realname_load_multiple(array $accounts) {
  $realnames = &drupal_static(__FUNCTION__, array());

  if ($new_accounts = array_diff_key($accounts, $realnames)) {
    // Attempt to fetch realnames from the database first.

    $realnames += db_query("SELECT uid, realname FROM {realname} WHERE uid IN (:uids)", array(':uids' => array_keys($new_accounts)))->fetchAllKeyed();

    // For each account that was not present in the database, generate its
    // real name.
    foreach ($new_accounts as $uid => $account) {
      if (!isset($realnames[$uid])) {
        $realnames[$uid] = realname_update($account);
      }
    }
  }

  return array_intersect_key($realnames, $accounts);
}

/**
 * Update the realname for a user account.
 *
 * @param $account
 *   A user account object.
 *
 * @see hook_realname_pattern_alter()
 * @see hook_realname_alter()
 * @see hook_realname_update()
 */
function realname_update(User $account) {
    // Get the default pattern and allow other modules to alter it.
  $config = config('realname.settings');
  $pattern = $config->get('pattern');
  \Drupal::moduleHandler()->alter('realname_pattern', $pattern, $account);

  // Perform token replacement on the real name pattern.
  $realname = \Drupal::token()->replace($pattern, array('user' => $account), array('clear' => TRUE, 'sanitize' => FALSE));

  // Remove any HTML tags.
  $realname = strip_tags(decode_entities($realname));

  // Remove double spaces (if a token had no value).
  $realname = preg_replace('/ {2,}/', ' ', $realname);

  // The name must be trimmed to 255 characters before inserting into the database.
  $realname = truncate_utf8(trim($realname), 255);

  // Allow other modules to alter the generated realname.
  \Drupal::moduleHandler()->alter('realname', $realname, $account);

  // Save to the database and the static cache.
  db_merge('realname')
    ->key(array('uid' => $account->id()))
    ->fields(array(
      'realname' => $realname,
      'created' => REQUEST_TIME,
    ))
    ->execute();

  // Allow modules to react to the realname being updated.
  \Drupal::moduleHandler()->invokeAll('realname_update', array($realname, $account));

  $account->setUsername($realname);
  return $realname;
}

/**
 * Delete a real name.
 *
 * @param $uid
 *   A user ID.
 */
function realname_delete($uid) {
  return realname_delete_multiple(array($uid));
}

/**
 * Delete multiple real names.
 *
 * @param $uids
 *   An array of user IDs.
 */
function realname_delete_multiple(array $uids) {
  db_delete('realname')->condition('uid', $uids, 'IN')->execute();
  drupal_static_reset('realname_load_multiple');
  entity_get_controller('user')->resetCache($uids);
}

/**
 * Delete all real names.
 */
function realname_delete_all() {
  db_delete('realname')->execute();
  drupal_static_reset('realname_load_multiple');
  entity_get_controller('user')->resetCache();
}

/**
 * @} End of "addtogroup realname".
 */
