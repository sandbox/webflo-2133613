<?php

/**
 * @file
 * Contains \Drupal\realname\RealnameServiceProvider.
 */

namespace Drupal\realname;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Defines the Autocomplete service provider.
 */
class RealnameServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides user_autocomplete service with realname implementation.
    // @todo: The global override of this service triggers all kinds of problems
    // e.g. node form validation fails becuase the entity builder uses the
    // formatter username.
    // Maybe this feature should be configurable?
    /*
    $definition = $container->getDefinition('user.autocomplete');
    $definition->setClass('Drupal\realname\RealnameAutocomplete');
    */
  }

}
