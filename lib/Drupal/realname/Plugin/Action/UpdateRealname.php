<?php

/**
 * @file
 * Contains \Drupal\realname\Plugin\Action\UpdateRealname.
 */

namespace Drupal\realname\Plugin\Action;

use Drupal\Core\Action\ActionBase;

/**
 * Update real name.
 *
 * @Action(
 *   id = "realname_update_action",
 *   label = @Translation("Update users real name"),
 *   type = "user"
 * )
 */
class UpdateRealname extends ActionBase {

  /**
   * Executes the plugin.
   */
  public function execute($account = NULL) {
    realname_delete($account->id());
  }

}
