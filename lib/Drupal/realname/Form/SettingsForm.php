<?php

/**
 * @file
 * Contains \Drupal\realname\Form\SettingsForm.
 */

namespace Drupal\realname\Form;

use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure real name settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'realname_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->configFactory->get('realname.settings');


    $form['pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Realname pattern'),
      '#default_value' => $config->get('pattern'),
      // @todo Implement token_element_validate, if token module will be ported to D8
      /*
      '#element_validate' => array('token_element_validate'),
      '#token_types' => array('user'),
      */
      '#min_tokens' => 1,
      '#required' => TRUE,
      '#maxlength' => 256,
    );

    // Add the token tree UI.
    // @todo Implementation of token_help, if token module will not be not ported to 8.x
    /*
    $form['token_help'] = array(
      '#theme' => 'token_help',
      '#token_types' => array('user'),
      '#global_types' => FALSE,
    );
    */

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    if (strpos($form_state['input']['pattern'], '[user:name]') !== FALSE) {
      form_set_error('realname_pattern', t('The <em>[user:name]</em> token cannot be used as it will cause recursion.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    if ($form['pattern']['#default_value'] != $form_state['values']['pattern']) {
      $config = $this->configFactory->get('realname.settings');
      $config->set('pattern', $form_state['values']['pattern'])->save();
      // Only clear the realname cache if the pattern was changed.
      realname_delete_all();
    };

    parent::submitForm($form, $form_state);
  }

}
