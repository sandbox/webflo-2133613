<?php

/**
 * @file
 * Contains \Drupal\realname\RealnameAutocomplete.
 */

namespace Drupal\realname;

use Drupal\user\UserAutocomplete;

/**
 * Defines a helper class to get user autocompletion results.
 */
class RealnameAutocomplete extends UserAutocomplete {

  public function getMatches($string, $include_anonymous = FALSE) {
    $matches = array();
    if ($string) {
      if ($include_anonymous) {
        $anonymous_name = $this->configFactory->get('user.settings')->get('anonymous');
        // Allow autocompletion for the anonymous user.
        if (stripos($anonymous_name, $string) !== FALSE) {
          $matches[$anonymous_name] = check_plain($anonymous_name);
        }
      }

      $query = $this->connection->select('users');
      $query->join('realname', 'rn', 'users.uid = rn.uid');

      $query
        ->fields('users', array('name'))
        ->fields('rn', array('realname'))
        ->condition('rn.realname', db_like($string) . '%', 'LIKE')
        ->range(0, 10);

      $result = $query->execute();
      foreach ($result as $account) {
        $matches[$account->name] = check_plain($account->realname);
      }
    }

    return $matches;
  }

}
