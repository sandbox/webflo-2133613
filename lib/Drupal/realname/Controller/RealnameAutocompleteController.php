<?php

/**
 * @file
 * Contains \Drupal\realname\Controller\RealnameAutocompleteController.
 */

namespace Drupal\realname\Controller;

use Drupal\user\Controller\UserAutocompleteController;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RealnameAutocompleteController extends UserAutocompleteController {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('realname.autocomplete')
    );
  }

}
