<?php

/**
 * @file
 * Views integration for the realname module.
 */

/**
 * Implements hook_views_data().
 */
function realname_views_data() {
  $data['realname']['table']['group']  = t('Realname');
  $data['realname']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['realname']['realname'] = array(
    'title' => t('Real name'),
    'help' => t("The user's real name."),
    'field' => array(
      'id' => 'user',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'argument' => array(
      'id' => 'string',
    ),
    'filter' => array(
      'id' => 'string',
      'title' => t('Name'),
      'help' => t("The user's real name. This filter does not check if the user exists and allows partial matching. Does not utilize autocomplete.")
    ),
  );

  return $data;
}
